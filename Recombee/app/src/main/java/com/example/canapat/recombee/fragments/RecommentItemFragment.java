package com.example.canapat.recombee.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.example.canapat.recombee.R;
import com.example.canapat.recombee.activities.ServiceActivity;
import com.example.canapat.recombee.configs.SharedData;
import com.example.canapat.recombee.models.adapters.RecommendedItemsAdapter;

public class RecommentItemFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private View view;
    private GridView recommendedItemGridview;
    private static RecommentItemFragment fragment;

    public static RecommentItemFragment getInstance(String param1, String param2) {
        RecommentItemFragment fragment = new RecommentItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static RecommentItemFragment getInstance() {
        if( fragment == null ){
            fragment = new RecommentItemFragment();
        }
        return fragment;
    }

    public RecommentItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void initInstances() {
        recommendedItemGridview = (GridView) view.findViewById(R.id.gridview);
        RecommendedItemsAdapter recommendedItemsAdapter = new RecommendedItemsAdapter(getContext(), SharedData.getItemList());
        recommendedItemGridview.setAdapter(recommendedItemsAdapter);
        recommendedItemGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), ServiceActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_recomment_item, container, false);
        initInstances();
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
