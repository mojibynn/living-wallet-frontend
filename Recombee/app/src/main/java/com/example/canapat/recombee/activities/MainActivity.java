package com.example.canapat.recombee.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.canapat.recombee.R;
import com.example.canapat.recombee.apis.RequestApi;
import com.example.canapat.recombee.configs.Configuration;
import com.example.canapat.recombee.configs.SharedData;
import com.example.canapat.recombee.fragments.RecommentItemFragment;
import com.example.canapat.recombee.utils.JsonUtils;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

	private String result = "";

	private TextView showText;

	private TextView mTextViewWallet;
	private TextView mTextViewGame;
	private TextView mTextViewHistory;
	private TextView mTextViewMe;

	private TextView toLoginPageBtn;
	private TextView toLogoutPageBtn;

	private LinearLayout sliding_balanceLogon;
	private TextView balance;

	private Fragment fragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initInstances();
	}

	private void initInstances() {
		showText = (TextView) findViewById(R.id.showText);
		mTextViewWallet = (TextView) findViewById(R.id.tabbar_menu_textview_1);
		mTextViewWallet.setOnClickListener(this);
		mTextViewWallet.setSelected(true);
		mTextViewGame = (TextView) findViewById(R.id.tabbar_menu_textview_2);
		mTextViewGame.setOnClickListener(this);
		mTextViewHistory = (TextView) findViewById(R.id.tabbar_menu_textview_3);
		mTextViewHistory.setOnClickListener(this);
		mTextViewMe = (TextView) findViewById(R.id.tabbar_menu_textview_4);
		mTextViewHistory.setOnClickListener(this);

		toLoginPageBtn = (TextView) findViewById(R.id.toLoginPageBtn);
		toLoginPageBtn.setOnClickListener(this);
		toLogoutPageBtn = (TextView) findViewById(R.id.toLogoutPageBtn);
		toLogoutPageBtn.setOnClickListener(this);

		sliding_balanceLogon = (LinearLayout) findViewById(R.id.sliding_balanceLogon);
		balance = (TextView) findViewById(R.id.sliding_balanceNumber);


	}

	private JsonHttpResponseHandler signOutHandler = new JsonHttpResponseHandler() {

		@Override
		public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
			super.onSuccess(statusCode, headers, response);
			Log.d("DebugHere","Sign Out");
		}

		@Override
		public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
			super.onFailure(statusCode, headers, responseString, throwable);
		}
	};

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.tabbar_menu_textview_1 :
				break;
			case R.id.toLoginPageBtn :
				Intent intent = new Intent(MainActivity.this, LoginActivity.class);
				startActivityForResult(intent, 1);
				break;
			case R.id.toLogoutPageBtn :
				logOut();
				break;
		}
	}

	private void logOut() {
		SharedData.setLogout();
		clearFragment(RecommentItemFragment.getInstance());
		toLoginPageBtn.setVisibility(View.VISIBLE);
		sliding_balanceLogon.setVisibility(View.GONE);
		toLogoutPageBtn.setVisibility(View.GONE);

		RequestApi.signOut(MainActivity.this, signOutHandler);
	}

	private void logIn() {
		RecommentItemFragment recommentItemFragment = RecommentItemFragment.getInstance();
		setFragment(recommentItemFragment);
		toLoginPageBtn.setVisibility(View.GONE);
		sliding_balanceLogon.setVisibility(View.VISIBLE);
		toLogoutPageBtn.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode,resultCode,data);

		if (requestCode == 1) {
			if(resultCode == Activity.RESULT_OK){
				boolean result = data.getBooleanExtra(Configuration.LOGIN_FLAG, false);
				if( SharedData.getLoggedIn() ) {
					logIn();
				}
			}
			if (resultCode == Activity.RESULT_CANCELED) {
				//Write your code if there's no result
			}
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		//No call for super(). Bug on API Level > 11.
	}

	private void setFragment(@NonNull Fragment fragment) {
		this.fragment = fragment;
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.replace(R.id.fragment, this.fragment);
		transaction.commit();
	}

	private void clearFragment(@NonNull Fragment fragment){
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.remove(fragment);
		transaction.commit();
	}
}
