package com.example.canapat.recombee.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.canapat.recombee.R;
import com.example.canapat.recombee.apis.RequestApi;
import com.example.canapat.recombee.configs.Configuration;
import com.example.canapat.recombee.configs.SharedData;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

	private EditText username;
	private EditText password;

	private Button loginBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initView();
	}

	private void initView() {
		username = (EditText) findViewById(R.id.username);
		password = (EditText) findViewById(R.id.password);

		loginBtn = (Button) findViewById(R.id.loginBtn);
		loginBtn.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.loginBtn :
				Log.d("Debughere","click");
				RequestApi.signIn(LoginActivity.this, signInHandler, username.getText().toString().trim(), password.getText().toString().trim());
				break;
		}
	}

	private JsonHttpResponseHandler signInHandler = new JsonHttpResponseHandler() {

		@Override
		public void onSuccess(int statusCode, Header headers[], JSONObject response) {
			super.onSuccess(statusCode, headers, response);

			Log.d("signInResult",response.toString());

			try {
				SharedData.setLogin(response);
			} catch (JSONException e) {
				e.printStackTrace();
			}
//			Log.d("Items", SharedData.getItemList().get(0).getItemId());

			Intent intent = new Intent();
			intent.putExtra(Configuration.LOGIN_FLAG,true);
			setResult(Activity.RESULT_OK,intent);
			finish();
		}

		@Override
		public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
			super.onFailure(statusCode, headers, responseString, throwable);
		}
	};

}
