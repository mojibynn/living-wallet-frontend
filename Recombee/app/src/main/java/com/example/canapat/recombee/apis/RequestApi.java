package com.example.canapat.recombee.apis;

import android.content.Context;
import android.util.Log;

import com.example.canapat.recombee.configs.Configuration;
import com.example.canapat.recombee.configs.SharedData;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

/**
 * Created by canapat on 8/17/2017 AD.
 */

public class RequestApi {

	public static AsyncHttpClient getReccommendForUser(Context context, JsonHttpResponseHandler callback, String userID) {

		AsyncHttpClient client = null;
		try {
			client = new AsyncHttpClient();
			client.get(context, Configuration.USER_RECOMMEND_URL+userID, callback);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return client;
	}

	public static AsyncHttpClient signIn(Context context, JsonHttpResponseHandler callback, String username, String password) {

		AsyncHttpClient client = null;
		try {

			JSONObject json = new JSONObject();
			json.put("username", username);
			json.put("password", password);
			StringEntity body = new StringEntity(json.toString(), HTTP.UTF_8);
			body.setContentType("application/json");

			client = new AsyncHttpClient();
//			client.post(context, Configuration.SIGN_IN_URL, body, "application/json", callback);
			client.post(context, Configuration.MOCKY, body, "application/json", callback);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return client;
	}

	public static AsyncHttpClient signOut(Context context, JsonHttpResponseHandler callback) {

		AsyncHttpClient client = null;
		try {
			client = new AsyncHttpClient();
			Log.d("AccessToken",SharedData.getAccessToken());

			JSONObject json = new JSONObject();
			StringEntity body = new StringEntity(json.toString(), HTTP.UTF_8);
			body.setContentType("application/json");

			client.addHeader(Configuration.SING_OUT_HEADER, SharedData.getAccessToken());
			client.post(context,Configuration.SIGN_OUT_URL ,body , "application/json", callback);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return client;
	}
}
