package com.example.canapat.recombee.configs;

/**
 * Created by canapat on 8/18/2017 AD.
 */

public class Configuration {
	public static String LOGIN_FLAG = "LOGIN_FLAG";

	public static String MOCKY = "http://www.mocky.io/v2/5996e4e5130000b7008b77d8";

	public static String USER_RECOMMEND_URL = "http://172.23.254.86:8080/api/v1/Recombee/get/userbase/";
	public static String SIGN_IN_URL = "http://172.23.254.86:8080/api/v1/truemoney/signin";
	public static String SIGN_OUT_URL = "http://172.23.254.86:8080/api/v1/truemoney/signout";

	public static String SING_OUT_HEADER = "access-token";
}
