package com.example.canapat.recombee.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.canapat.recombee.R;
import com.example.canapat.recombee.views.RoundImageView;

public class ServiceActivity extends AppCompatActivity implements View.OnClickListener {

	private TextView tvTitle;
	private ImageView ivBack;

	private LinearLayout bottomRecommendedItem1;
	private TextView tvRecommendedItem1;
	private RoundImageView rivRecommendedItem1;
	private LinearLayout bottomRecommendedItem2;
	private TextView tvRecommendedItem2;
	private RoundImageView rivRecommendedItem2;
	private LinearLayout bottomRecommendedItem3;
	private TextView tvRecommendedItem3;
	private RoundImageView rivRecommendedItem3;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service);
		initInstances();
	}

	private void initInstances() {
		tvTitle = (TextView) findViewById(R.id.toolbar_title);
		ivBack = (ImageView) findViewById(R.id.toolbar_left_imageview);
		ivBack.setVisibility(View.VISIBLE);
		ivBack.setOnClickListener(this);
		String url = "https://s3-ap-southeast-1.amazonaws.com/mobile-resource.tewm-alpha/wallet-app/consumer/home/images/new_theme/landing/icon/wecard%403x.png";
		bottomRecommendedItem1 = (LinearLayout) findViewById(R.id.bottom_recommended_item_1);
		bottomRecommendedItem1.setOnClickListener(this);
		tvRecommendedItem1 = (TextView) findViewById(R.id.bottom_recommended_item_text_1);
		tvRecommendedItem1.setText("บัตร WeCard");
		rivRecommendedItem1 = (RoundImageView) findViewById(R.id.bottom_recommended_item_image_1);
		Glide.with(ServiceActivity.this).load(url).into(rivRecommendedItem1);
		bottomRecommendedItem2 = (LinearLayout) findViewById(R.id.bottom_recommended_item_2);
		bottomRecommendedItem2.setOnClickListener(this);
		tvRecommendedItem2 = (TextView) findViewById(R.id.bottom_recommended_item_text_2);
		tvRecommendedItem2.setText("บัตร WeCard");
		rivRecommendedItem2 = (RoundImageView) findViewById(R.id.bottom_recommended_item_image_2);
		Glide.with(ServiceActivity.this).load(url).into(rivRecommendedItem2);
		bottomRecommendedItem3 = (LinearLayout) findViewById(R.id.bottom_recommended_item_3);
		bottomRecommendedItem3.setOnClickListener(this);
		tvRecommendedItem3 = (TextView) findViewById(R.id.bottom_recommended_item_text_3);
		tvRecommendedItem3.setText("บัตร WeCard");
		rivRecommendedItem3 = (RoundImageView) findViewById(R.id.bottom_recommended_item_image_3);
		Glide.with(ServiceActivity.this).load(url).into(rivRecommendedItem3);
	}


	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.toolbar_left_imageview:
				goBack();
				break;
			case R.id.bottom_recommended_item_1:
				startNewService();
				break;
			case R.id.bottom_recommended_item_2:
				startNewService();
				break;
			case R.id.bottom_recommended_item_3:
				startNewService();
				break;
		}
	}

	private void startNewService() {
		Intent intent = new Intent(ServiceActivity.this, ServiceActivity.class);
		startActivity(intent);
		overridePendingTransition(R.anim.trans_left_in, R.anim.trans_left_out);
		finish();
	}

	private void goBack() {
		finish();
	}
}
