package com.example.canapat.recombee.configs;

import android.util.Log;

import com.example.canapat.recombee.models.RecommendItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by wachirapong.pra on 8/18/2017 AD.
 */

public class SharedData {

    private static String userId = "";
    private static String age = "";
    private static String gender = "";
    private static String occupation = "";
    private static String district = "";
    private static String zone = "";
    private static String province = "";
    private static Boolean loggedIn = false;
    private static String accessToken = "";

    private static ArrayList<RecommendItem> itemList = new ArrayList<RecommendItem>();

    // GET DATA
    public static String getUserId() {
        return userId;
    }

    public static String getAge() {
        return age;
    }

    public static String getGender() {
        return gender;
    }

    public static String getOccupation() {
        return occupation;
    }

    public static String getDistrict() {
        return district;
    }

    public static String getZone() {
        return zone;
    }

    public static String getProvince() {
        return province;
    }

    public static Boolean getLoggedIn() {
        return loggedIn;
    }

    public static String getAccessToken() {
        return accessToken;
    }

    // SET DATA
    public static void setUserId(String userId) {
        SharedData.userId = userId;
    }

    public static void setAge(String age) {
        SharedData.age = age;
    }

    public static void setGender(String gender) {
        SharedData.gender = gender;
    }

    public static void setOccupation(String occupation) {
        SharedData.occupation = occupation;
    }

    public static void setDistrict(String district) {
        SharedData.district = district;
    }

    public static void setZone(String zone) {
        SharedData.zone = zone;
    }

    public static void setProvince(String province) {
        SharedData.province = province;
    }

    public static void setLoggedIn(Boolean loggedIn) {
        SharedData.loggedIn = loggedIn;
    }

    public static void setAccessToken(String accessToken) {
        SharedData.accessToken = accessToken;
    }

    public static void setLogin(JSONObject data) throws JSONException {
        String year_string = data.getJSONObject("data").getString("birthdate").substring(0, 4);
        Log.d("YEAR", year_string);
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int age = year - Integer.parseInt(year_string);
        SharedData.age = String.valueOf(age);
        Log.d("AGE", SharedData.age);

        String title = data.getJSONObject("data").getString("title");
        SharedData.gender = (title.equals("นาย") || title.equals("ด.ช.")) ? "male" : "female";
        Log.d("GENDER", SharedData.gender);

        SharedData.userId = data.getJSONObject("data").getString("tmn_id");
        SharedData.occupation = data.getJSONObject("data").getString("occupation");

        SharedData.district = "แขวง";//data.getJSONObject("data").getString("แขวง");
        SharedData.zone = "เขต";//data.getJSONObject("data").getString("เขต");
        SharedData.province = "จังหวัด";//data.getJSONObject("data").getString("จังหวัด");

        SharedData.accessToken = data.getJSONObject("data").getString("access_token");
        SharedData.setItemList(data.getJSONObject("data").getJSONArray("recommendation_and_images"));

        SharedData.loggedIn = true;
    }

    public static void setLogout() {
        SharedData.userId = "";
        SharedData.age = "";
        SharedData.gender = "";
        SharedData.province = "";
        SharedData.zone = "";
        SharedData.district = "";
        SharedData.occupation = "";
        SharedData.accessToken = "";
        SharedData.loggedIn = false;
    }

    private static void setItemList(JSONArray dataArr) throws JSONException {
        for (int i = 0; i < dataArr.length(); i++) {
            JSONObject data = dataArr.getJSONObject(i).getJSONObject("recommendation");
            RecommendItem item = new RecommendItem();
            item.setItemId(data.getJSONObject("values").getString("itemId"));
            item.setName(data.getJSONObject("values").getString("name"));
            item.setLogoUrl(dataArr.getJSONObject(i).getString("logoUrl"));
            itemList.add(item);
        }
    }

    public static ArrayList<RecommendItem> getItemList() {
        return itemList;
    }

}
