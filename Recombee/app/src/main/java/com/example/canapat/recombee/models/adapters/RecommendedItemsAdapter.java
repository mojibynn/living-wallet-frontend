package com.example.canapat.recombee.models.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.canapat.recombee.R;
import com.example.canapat.recombee.models.RecommendItem;
import com.example.canapat.recombee.views.RoundImageView;

import java.util.ArrayList;

/**
 * Created by wachirapong.pra on 8/18/2017 AD.
 */

public class RecommendedItemsAdapter extends BaseAdapter {

	private Context mContext;
	private ArrayList<RecommendItem> items;

	public RecommendedItemsAdapter(Context context, ArrayList<RecommendItem> items){
		this.mContext = context;
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int i) {
		return items.get(i);
	}

	@Override
	public long getItemId(int i) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater mInflater =
				(LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if(view == null) {
			view = mInflater.inflate(R.layout.recommended_item, parent, false);
		}

		RecommendItem item = items.get(position);

//		String url = "https://s3-ap-southeast-1.amazonaws.com/mobile-resource.tewm-alpha/wallet-app/consumer/home/images/new_theme/landing/icon/wecard%403x.png";
		String url = item.getLogoUrl();
		RoundImageView roundImageView = (RoundImageView)view.findViewById(R.id.custom_recommended_item_image);
		Glide
				.with(mContext)
				.load(url)
//				.centerCrop()
//				.placeholder(R.drawable.loading_spinner)
				.into(roundImageView);
		TextView textView = (TextView) view.findViewById(R.id.custom_recommended_item_text);
//		textView.setText("บัตร\nWeCard");
		textView.setText(item.getName());
		return view;
	}

}
