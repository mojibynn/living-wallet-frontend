package com.example.canapat.recombee.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.example.canapat.recombee.R;


public class RoundImageView extends AppCompatImageView {
	public static final int CORNER_NONE = 0;
    public static final int CORNER_TOP_LEFT = 1;
    public static final int CORNER_TOP_RIGHT = 2;
    public static final int CORNER_BOTTOM_RIGHT = 4;
    public static final int CORNER_BOTTOM_LEFT = 8;
    public static final int CORNER_ALL = 15;

    private final RectF cornerRect = new RectF();
    private final Path path = new Path();
    private int cornerRadius;
    private int roundedCorners;
    private int shapeType = 1;

    public RoundImageView(Context context) {
        this(context, null);
    }

    public RoundImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RoundImageView);
        cornerRadius = a.getDimensionPixelSize(R.styleable.RoundImageView_cornerRadius, 0);
        roundedCorners = a.getInt(R.styleable.RoundImageView_roundedCorners, CORNER_NONE);
        shapeType = 1;
        a.recycle();
    }

    public void setCornerRadius(int radius) {
        if (cornerRadius != radius) {
            cornerRadius = radius;
            setPath();
            invalidate();
        }
    }

    public int getCornerRadius() {
        return cornerRadius;
    }

    public void setRoundedCorners(int corners) {
        if (roundedCorners != corners) {
            roundedCorners = corners;
            setPath();
            invalidate();
        }
    }

    public boolean isCornerRounded(int corner) {
        return (roundedCorners & corner) == corner;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR1) {
            try {
                if (!path.isEmpty()) {
                    canvas.clipPath(path);
                }
            } catch(UnsupportedOperationException e) {
//                Crashlytics.logException(e);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }

        super.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        setPath();
    }

    private void setPath() {
        path.rewind();

        if (cornerRadius >= 1f && roundedCorners != CORNER_NONE) {
            final int width = getWidth();
            final int height = getHeight();
            final float twoRadius = cornerRadius * 2;

            if (shapeType == 1) { //rectangle
                cornerRect.set(0, 0, twoRadius, twoRadius);

                if (isCornerRounded(CORNER_TOP_LEFT)) {
                    path.arcTo(cornerRect, 180f, 90f);
                }
                else {
                    path.moveTo(0f, 0f);
                }

                if (isCornerRounded(CORNER_TOP_RIGHT)) {
                    cornerRect.offsetTo(width - twoRadius, 0f);
                    path.arcTo(cornerRect, 270f, 90f);
                }
                else {
                    path.lineTo(width, 0f);
                }

                if (isCornerRounded(CORNER_BOTTOM_RIGHT)) {
                    cornerRect.offsetTo(width - twoRadius, height - twoRadius);
                    path.arcTo(cornerRect, 0f, 90f);
                }
                else {
                    path.lineTo(width, height);
                }

                if (isCornerRounded(CORNER_BOTTOM_LEFT)) {
                    cornerRect.offsetTo(0f, height - twoRadius);
                    path.arcTo(cornerRect, 90f, 90f);
                }
                else {
                    path.lineTo(0f, height);
                }
            }

            path.close();
        }
    }
}