package com.example.canapat.recombee.models;

import org.json.JSONObject;

/**
 * Created by panchorn on 8/18/2017 AD.
 */

public class RecommendItem {
    private String itemId = "";
    private String name = "";
    private String logoUrl = "";

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

}
