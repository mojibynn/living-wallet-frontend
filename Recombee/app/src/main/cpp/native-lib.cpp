#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_canapat_recombee_Main2Activity_stringFromJNI(JNIEnv* env, jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jstring JNICALL Java_com_example_canapat_recombee_Main2Activity_getApi(JNIEnv* env, jobject instance, jstring keyhash) {
std::string hello = "Hello from C++";
    std::string result;
    std::string str1 = "7eqeZHXMbmoSmPoVqlNhyuZujVg=";

    const char * c = str1.c_str();
    const char * str2 = env->GetStringUTFChars(keyhash,NULL);

    if(strcmp(str2, c) == 0) {
        result = "Correct";
    } else {
        result = "Incorrect";
    }
return env->NewStringUTF(result.c_str());
}
